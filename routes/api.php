<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// http://laravel-api.test/api/hello
// http://localhost/laravel-api/public/api/hello
Route::get('/hello', function() {
    echo 'Hello';
});

Route::group(['middleware' => 'auth:sanctum'], function() {
    // insert a new product
    // laravel-api.test/create
    Route::post('/create', [ProductController::class, 'create']);
    // laravel-api.test/show-all
    Route::put('/update/{id}', [ProductController::class, 'update']);
    Route::delete('/delete/{id}', [ProductController::class, 'delete']);
});

Route::get('/show-all', [ProductController::class, 'showAll']);
// Route::post('/create-public', [ProductController::class, 'create']);
// Route::delete('/delete-public/{id}', [ProductController::class, 'delete']);
// Route::put('/update-public/{id}', [ProductController::class, 'update']);
Route::get('/password', [AuthController::class, 'password']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    //return $request->user();

});

// https://gitlab.com/azman1204/laravel-api
