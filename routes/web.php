<?php

use Illuminate\Support\Facades\Route;
use App\Models\Product;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/day3-1', function () {
    return view('tutorial.day3');
});

Route::get('/product', function () {
    $products = Product::all();
    $test = "Test Value";
    return view('tutorial.tutorial_day2',compact('products','test'));
});

Route::get('/product-html', function () {
    $products = Product::all();
    $test = "Test Value";
    return view('tutorial.tutorial_day2',compact('products','test'));
});