<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'product';
    // column allow to insert and update
    protected $fillable = ['name', 'description', 'price', 'slug'];
    // table product colum created_at and updated_at
    public $timestamps = false;
}
