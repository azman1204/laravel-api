<?php
namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    // insert data into product table
    public function create(Request $req)
    {
        // mass assignment
        $req->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'slug' => 'required',
        ]);
        Product::create($req->all());
    }

    public function showAll()
    {
        return Product::all();
        // return response()->json(['status'=>200,'data'=>Product::all()]);
    }

    public function update($id, Request $req)
    {
        $req->validate([
            'name' => 'required',
            'price' => 'required|numeric',
            'slug' => 'required',
        ]);
        $product = Product::find($id);
        $product->update($req->all());
        return $product;
    }

    public function delete($id)
    {
        return Product::destroy($id);
    }
}
