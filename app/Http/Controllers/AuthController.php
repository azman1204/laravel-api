<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthController extends Controller {
    function login(Request $req) {
        $user = User::where('email', $req->email)->first();
        if ($user) {
            if (Hash::check($req->password, $user->password)) {
                // email & pwd correct
                $token = $user->createToken('mytoken')->plainTextToken;
                return $token;
            } else {
                return 'gagal';
            }
        } else {
            return response(['message' => 'Invalid username or password'], 201);
        }
    }

    function logout(Request $request) {
        // auth()->user()->tokens()->delete();
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $user->tokens()->delete();
            return ['message' => 'Logged Out'];
        } else {
            return ['message' => 'Invalid user'];
        }

    }

    // temporary
    function password() {
        return Hash::make('1234');
    }
}
