<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Day 3</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" 
    rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.1.js"></script>
</head>
<body>
<h1>Day 3</h1>
<span id='userName'>Guest</span>
<input type="hidden" name='usertoken' id='usertoken'>
<div id="loginScreen">
<div class="mb-3">
        <label for="username" class="form-label">ID Pengguna</label>
        <input type="text" class="form-control" id="username" placeholder="raj@gmail.com">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Katalaluan</label>
        <input type="password" class="form-control" id="password">
    </div>
    <button type="button" id="btnlogin" class="btn btn-success">Log Masuk</button>
    <button type="button" id="btnlogout" style="display:none" class="btn btn-warning">Log Keluar</button>
</div>

<div id="borangProduk" style="display:none">
    <div class="mb-3">
        <label for="productNm" class="form-label">Nama Produk</label>
        <input type="text" class="form-control" id="productNm" placeholder="ACER Printer">
    </div>
    <div class="mb-3">
        <label for="productSlug" class="form-label">Slug</label>
        <input type="text" class="form-control" id="productSlug" placeholder="acer-printer">
    </div>

    <div class="mb-3">
        <label for="productPrice" class="form-label">Harga</label>
        <input type="text" class="form-control" id="productPrice">
    </div>

    <div class="mb-3">
    <label for="productDescr" class="form-label">Penerangan</label>
    <textarea class="form-control" id="productDescr" rows="3"></textarea>
    </div>
    <button type="button" id="btnsave" class="btn btn-success">Simpan</button>
</div>


<div id='senaraiProduk' style="display:none">
    <button type="button" id="btnadd" class="btn btn-primary">Tambah Produk</button>
    <table class="table table-striped">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Produk</th>
        <th scope="col">Harga</th>
        <th scope="col">Tindakan</th>
        </tr>
    </thead>
    <tbody id='prodbody'>
    </tbody>
    </table> 
</div>
   
</body>
</html>
<script>
$('#btnlogout').click(function(){
    
    let token = document.querySelector('meta[name="csrf-token"]').content;

    $.ajax({
        type: "POST",
        beforeSend: function(request) {
            request.setRequestHeader("X-CSRF-Token", token);
            request.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
            request.setRequestHeader("Accept", 'application/json');
        },
        url: "http://laravel-api.test/api/logout",
        data: {
                email: $('#username').val()
            },
        
        success: function(msg) {
            location.href = '/day3-1';
        }
        });
});

    $('#btnlogin').click(function(){
        if ($('#username').val() == '' || $('#password').val() == ''){
            alert('Sila masukkan ID Pengguna & Katalaluan');
            return false;
        }

        let token = document.querySelector('meta[name="csrf-token"]').content;

        $.ajax({
            type: "POST",
            beforeSend: function(request) {
                request.setRequestHeader("X-CSRF-Token", token);
                request.setRequestHeader("Content-type", 'application/x-www-form-urlencoded');
                request.setRequestHeader("Accept", 'application/json');
            },
            url: "http://laravel-api.test/api/login",
            data: {
                    email: $('#username').val(),
                    password: $('#password').val()
                },
            
            success: function(msg) {
                if (msg != 'gagal'){
                    // console.log(msg.message);
                    if (msg.message != undefined && msg.message != ''){
                        alert('Sila masukkan ID Pengguna & Katalaluan yang betul'); 
                    }else{
                        $('#userName').html($('#username').val());
                        // $('#loginScreen').hide();
                        $('#usertoken').val(msg);
                        $('#btnlogin').hide();
                        $('#btnlogout').show();
                        retrieveData();
                        $('#senaraiProduk').show();                        
                    }
                    

                }else{
                    alert('Sila masukkan ID Pengguna & Katalaluan yang betul');
                }
                
            }
            });

        
    });

    $('#btnsave').click(function(){
        let productNm = $('#productNm').val();
        let productSlug = $('#productSlug').val();
        let productPrice = $('#productPrice').val();
        let productDescr = $('#productDescr').val();
        
        let data = 'name='+productNm+'&slug='+productSlug+'&price='+productPrice+'&description='+productDescr;
        const xhttp = new XMLHttpRequest();
        let token = document.querySelector('meta[name="csrf-token"]').content;
        xhttp.onreadystatechange = function() {
            // console.log(this.readyState);
            // console.log(this.status);
            if (this.readyState == 4 && this.status == 200) {
                $('#borangProduk').hide();
                $('#senaraiProduk').show();

                $('#productNm').val('');
                $('#productSlug').val('');
                $('#productPrice').val('');
                $('#productDescr').val('');
                retrieveData();
            }};
        xhttp.open("POST", "http://laravel-api.test/api/create");
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.setRequestHeader("Authorization", 'Bearer '+$('#usertoken').val());
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.setRequestHeader("X-CSRF-Token", token);
        xhttp.send(data);

    });
    
    $('#btnadd').click(function(){
        // alert('Show Form JQuery Style');
        $('#borangProduk').show();
        $('#senaraiProduk').hide();
        // document.getElementById('senaraiProduk').style.display='none';
    })

    // console.log(document.getElementById('btnadd').innerHTML);
    // console.log($('#btnadd').html());

function deleteRecord(id){
    if (confirm('Anda pasti?')){
        const xhttp = new XMLHttpRequest();
        let token = document.querySelector('meta[name="csrf-token"]').content;
        xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            retrieveData();
        }};
        alert($('#usertoken').val());
        xhttp.open("DELETE", "http://laravel-api.test/api/delete/"+id);
        xhttp.setRequestHeader("Authorization", 'Bearer '+$('#usertoken').val());
        xhttp.setRequestHeader("Accept", "application/json");
        xhttp.setRequestHeader("X-CSRF-Token", token);
        xhttp.send();
    }
}
function retrieveData(){
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let data = JSON.parse(this.responseText);
            let str = '';
            let i = 1;
            for(rec in data){                
                str += '<tr>';
                str += '<th scope="row">'+ parseInt(i++) +'</th>';
                str += '<td>'+data[rec]['name']+'</td>';
                str += '<td>'+data[rec]['price']+'</td>';
                str += '<td><button type="button" class="btn btn-info">Kemaskin</button> ';
                str += '<button type="button" onclick="deleteRecord('+data[rec]['id']+')" class="btn btn-danger">Hapus</button></td>';
                str += '</tr>';
            }
            document.getElementById('prodbody').innerHTML = str;
        }
    };
    xhttp.open("GET", "http://laravel-api.test/api/show-all");
    xhttp.send();
}   

</script>