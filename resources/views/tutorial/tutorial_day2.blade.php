<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Javascript Tutorial on Blade</title>
</head>
<body>
    <input type="text" id="action" onblur="changeLabel()" value="add">
    
    <div id="borang_produk">
        <h1 id="bp_title"></h1>
        <form action="#" id="form_produk" method="POST">
            Nama Produk<input type="text" name="name">
            <br>
            Harga <input type="text" name="price">
            <br>
            Penerangan <textarea name="description" id="description" cols="30" rows="10"></textarea>
            <br>
            <input type="button" onclick="submitForm()" value="Simpan">
            <input type="button" value="Set Semula">
        </form>        
    </div>

    <div id="senarai_produk">
        <h1>Senarai Product</h1>
        <table border="1">
            <tr>
                <th>Bil</th>
                <th><input type="checkbox" onclick="checkAll()" name="checkAll" id="checkAll"></th>
                <th>Nama produk</th>
                <th>Tindakan</th>
            </tr>
            @foreach($products as $p)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <th><input type="checkbox" name="product_id" value="{{$p->id}}"></th>
                <td>{{$p->name}}</td>
                <td>
                    <input type="button" value="Kemaskini">
                    <input type="button" onclick="deleteRecord({{$p->id}})" value="Hapus">
                </td>
            </tr>
            @endforeach
            
        </table>
    </div>

    
</body>
</html>
<script>
    

    function submitForm(){
        // alert('test');
        let f = document.getElementById('form_produk');
        console.log(f.price.value);
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
        
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
         }
        }
    xhttp.open("POST", "http://laravel-api.test:8081/api/create-public",true);
    // xhttp.open("GET", "/api/show-all",true);
    // xhttp.open("GET", "https://jsonplaceholder.typicode.com/todos",true);
    // xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();

    }
    function checkAll(){
        
        let checkall = document.getElementById('checkAll').checked;

        let rows = document.getElementsByName('product_id');
        for (let i=0; i < rows.length;i++){
            document.getElementsByName('product_id')[i].checked = checkall;
        }
        
    }

    function deleteRecord(id){
        let i = confirm("Anda Pasti untuk delete Record "+id);
        console.log(i);
    }

    function changeLabel(){
        let action = document.getElementById("action").value;
        console.log(action);
        if (action == "add")
            document.getElementById("bp_title").innerHTML = "Tambah Produk";
        else
            document.getElementById("bp_title").innerHTML = "Kemaskini Produk";
    }

    changeLabel();
    // alert('<?php // echo $test1;?>');

    function retrieveProduct(){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
        
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
         }
        }
    xhttp.open("GET", "http://laravel-api.test:8081/api/show-all",true);
    // xhttp.open("GET", "/api/show-all",true);
    // xhttp.open("GET", "https://jsonplaceholder.typicode.com/todos",true);
    // xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}
retrieveProduct();
    
</script>