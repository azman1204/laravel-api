<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Javascript Tutorial</title>
</head>
<body>
    <input type="text" id="action" onblur="changeLabel()" value="add">
    
    <div id="borang_produk">
        <h1 id="bp_title"></h1>
        <form action="/api/create" method="POST">
            Nama Produk<input type="text">
            <br>
            Harga <input type="text">
            <br>
            Penerangan <textarea name="description" id="description" cols="30" rows="10"></textarea>
            <br>
            <input type="button" value="Simpan">
            <input type="button" value="Set Semula">
        </form>        
    </div>

    <div id="senarai_produk">
        <h1>Senarai Product</h1>
        <table border="1">
            <tr>
                <th>Bil</th>
                <th>Nama produk</th>
                <th>Tindakan</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Asus Notebook</td>
                <td>
                    <input type="button" value="Kemaskini">
                    <input type="button" value="Hapus">
                </td>
            </tr>
        </table>
    </div>

    
</body>
</html>
<script>
    
    function changeLabel(){
        let action = document.getElementById("action").value;
        if (action == "add")
            document.getElementById("bp_title").innerHTML = "Tambah Produk";
        else
            document.getElementById("bp_title").innerHTML = "Kemaskini Produk";
    }

    changeLabel();
    
</script>